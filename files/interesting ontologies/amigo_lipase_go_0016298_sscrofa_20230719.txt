gene_name	product	interest
SMPDL3B	Sphingomyelin phosphodiesterase C-terminal domain-containing protein	lipid biosynthesis
PLCL2	Phosphoinositide phospholipase C	lipid biosynthesis
PNPLA7	Patatin-like phospholipase domain-containing protein 7 isoform X4	lipid biosynthesis
PNLIPRP1	Triacylglycerol lipase	lipid biosynthesis
PNLIPRP1	Triacylglycerol lipase	lipid biosynthesis
PLCZ1	Phosphoinositide phospholipase C	lipid biosynthesis
PLCZ1	Phosphoinositide phospholipase C	lipid biosynthesis
LIPE	Hormone-sensitive lipase	lipid biosynthesis
PLCL1	Phosphoinositide phospholipase C	lipid biosynthesis
PLCL1	Phosphoinositide phospholipase C	lipid biosynthesis
FAAH	fatty acid amide hydrolase	lipid biosynthesis
PNLIPRP2	Pancreatic lipase-related protein 2	lipid biosynthesis
PLA2G4A	Phospholipase A2	lipid biosynthesis
DAGLB	sn-1-specific diacylglycerol lipase	lipid biosynthesis
LIPC	Hepatic triacylglycerol lipase	lipid biosynthesis
LIPC	Hepatic triacylglycerol lipase	lipid biosynthesis
LIPC	Hepatic triacylglycerol lipase	lipid biosynthesis
LIPC	Hepatic triacylglycerol lipase	lipid biosynthesis
ABHD16A	AB hydrolase-1 domain-containing protein	lipid biosynthesis
PNPLA2	PNPLA domain-containing protein	lipid biosynthesis
ABHD3	AB hydrolase-1 domain-containing protein	lipid biosynthesis
ABHD3	AB hydrolase-1 domain-containing protein	lipid biosynthesis
PLA2G1B	Phospholipase A2	lipid biosynthesis
PLA1A	Lipase domain-containing protein	lipid biosynthesis
PLCB1	1-phosphatidylinositol 4,5-bisphosphate phosphodiesterase	lipid biosynthesis
ENPP7	Ectonucleotide pyrophosphatase/phosphodiesterase 7	lipid biosynthesis
LIPE	Hormone-sensitive lipase	lipid biosynthesis
LIPE	Hormone-sensitive lipase	lipid biosynthesis
LIPE	Hormone-sensitive lipase	lipid biosynthesis
GDE1	GP-PDE domain-containing protein	lipid biosynthesis
PLA2G4F	Phospholipase A2	lipid biosynthesis
CCR1	G-protein coupled receptors family 1 profile domain-containing protein	lipid biosynthesis
CDT1	PI-PLC Y-box domain-containing protein	lipid biosynthesis
PLCH2	Phosphoinositide phospholipase C	lipid biosynthesis
PLCH2	Phosphoinositide phospholipase C	lipid biosynthesis
LIPG	Lipoprotein lipase	lipid biosynthesis
PNPLA3	PNPLA domain-containing protein	lipid biosynthesis
PLCD4	1-phosphatidylinositol 4,5-bisphosphate phosphodiesterase delta-4	lipid biosynthesis
PLCD4	1-phosphatidylinositol 4,5-bisphosphate phosphodiesterase delta-4	lipid biosynthesis
PLB1	Phospholipase B1, membrane-associated	lipid biosynthesis
PNLIP	Pancreatic triacylglycerol lipase	lipid biosynthesis
PLCH1	Phosphoinositide phospholipase C	lipid biosynthesis
ABHD6	AB hydrolase-1 domain-containing protein	lipid biosynthesis
PLD1	Phospholipase	lipid biosynthesis
SMPD4	Sphingomyelin phosphodiesterase 4 isoform 2	lipid biosynthesis
ABHD2	AB hydrolase-1 domain-containing protein	lipid biosynthesis
PLA2G6	phospholipase A2	lipid biosynthesis
PLA2G5	Phospholipase A2	lipid biosynthesis
PLD6	PLD phosphodiesterase domain-containing protein	lipid biosynthesis
PNLIPRP3	Triacylglycerol lipase	lipid biosynthesis
PNLIPRP3	Triacylglycerol lipase	lipid biosynthesis
PNLIPRP2	Triacylglycerol lipase	lipid biosynthesis
PNLIPRP2	Triacylglycerol lipase	lipid biosynthesis
PLA2G7	Platelet-activating factor acetylhydrolase	lipid biosynthesis
LYPLA2	palmitoyl-protein hydrolase	lipid biosynthesis
PLD2	Phospholipase	lipid biosynthesis
LPL	Lipoprotein lipase	lipid biosynthesis
LPL	Lipoprotein lipase	lipid biosynthesis
LPL	Lipoprotein lipase	lipid biosynthesis
LPL	Lipoprotein lipase	lipid biosynthesis
LPL	Lipoprotein lipase	lipid biosynthesis
LPL	Lipoprotein lipase	lipid biosynthesis
NOTUM	Notum, palmitoleoyl-protein carboxylesterase	lipid biosynthesis
PROCA1	Phospholipase A2 domain-containing protein	lipid biosynthesis
PLA2G3	Phospholipase A2 domain-containing protein	lipid biosynthesis
PLA2G3	Phospholipase A2 domain-containing protein	lipid biosynthesis
PLA2G12A	Phospholipase A2 group XIIA	lipid biosynthesis
PRDX6	Peroxiredoxin-6	lipid biosynthesis
PRDX6	Peroxiredoxin-6	lipid biosynthesis
PLAAT5	LRAT domain-containing protein	lipid biosynthesis
PLAAT5	LRAT domain-containing protein	lipid biosynthesis
PNLIP	Triacylglycerol lipase	lipid biosynthesis
PNLIP	Triacylglycerol lipase	lipid biosynthesis
PLCD1	Phosphoinositide phospholipase C	lipid biosynthesis
PLBD1	Phospholipase B-like	lipid biosynthesis
PLA2G12B	Phospholipase A2 group XIIB	lipid biosynthesis
LIPI	Lipase domain-containing protein	lipid biosynthesis
PLA2G15	Group XV phospholipase A2	lipid biosynthesis
PLA2G15	Group XV phospholipase A2	lipid biosynthesis
PLA2G15	Group XV phospholipase A2	lipid biosynthesis
PLA2G15	Group XV phospholipase A2	lipid biosynthesis
LCAT	Lecithin-cholesterol acyltransferase	lipid biosynthesis
SMPDL3A	Acid sphingomyelinase-like phosphodiesterase	lipid biosynthesis
PLCD3	Phosphoinositide phospholipase C	lipid biosynthesis
PLCZ	1-phosphatidylinositol 4,5-bisphosphate phosphodiesterase zeta-1	lipid biosynthesis
PLCZ	1-phosphatidylinositol 4,5-bisphosphate phosphodiesterase zeta-1	lipid biosynthesis
PLCE1	Phosphoinositide phospholipase C	lipid biosynthesis
PLCE1	Phosphoinositide phospholipase C	lipid biosynthesis
LPL	Lipoprotein lipase	lipid biosynthesis
LPL	Lipoprotein lipase	lipid biosynthesis
LDAH	Lipid droplet-associated hydrolase	lipid biosynthesis
CES1	Carboxylesterase type B domain-containing protein	lipid biosynthesis
LIPE	Hormone-sensitive lipase	lipid biosynthesis
LIPE	Hormone-sensitive lipase	lipid biosynthesis
LPL	Lipoprotein lipase	lipid biosynthesis
LPL	Lipoprotein lipase	lipid biosynthesis
LIPE	Hormone-sensitive lipase	lipid biosynthesis
LIPE	Hormone-sensitive lipase	lipid biosynthesis
PLA2G4E	Phospholipase A2	lipid biosynthesis
PNPLA4	PNPLA domain-containing protein	lipid biosynthesis
PLCH1	Phosphoinositide phospholipase C	lipid biosynthesis
SMPD4	Sphingomyelin phosphodiesterase 4	lipid biosynthesis
DAGLB	Diacylglycerol lipase beta	lipid biosynthesis
LPL	Lipoprotein lipase	lipid biosynthesis
LPL	Lipoprotein lipase	lipid biosynthesis
PLA2G1B	Phospholipase A2	lipid biosynthesis
PNPLA7	Patatin like phospholipase domain containing 7	lipid biosynthesis
ABHD16A	Abhydrolase domain containing 16A, phospholipase	lipid biosynthesis
ABHD16A	Abhydrolase domain containing 16A, phospholipase	lipid biosynthesis
ABHD15	Abhydrolase domain containing 15	lipid biosynthesis
GDPD1	GP-PDE domain-containing protein	lipid biosynthesis
A0A5G2R3N8	Uncharacterized protein	lipid biosynthesis
PLCB3	1-phosphatidylinositol 4,5-bisphosphate phosphodiesterase	lipid biosynthesis
DDHD1	DDHD domain containing 1	lipid biosynthesis
PLA2G1B	Phospholipase A2, major isoenzyme	lipid biosynthesis
PNLIP	Pancreatic triacylglycerol lipase	lipid biosynthesis
PNPLA6	Patatin like phospholipase domain containing 6	lipid biosynthesis
MGLL	Monoglyceride lipase	lipid biosynthesis
ABHD16B	Abhydrolase domain containing 16B	lipid biosynthesis
ABHD16B	Abhydrolase domain containing 16B	lipid biosynthesis
PLAAT5	LRAT domain-containing protein	lipid biosynthesis
PLAAT5	LRAT domain-containing protein	lipid biosynthesis
PLAAT1	Phospholipase A and acyltransferase 1	lipid biosynthesis
PLAAT1	Phospholipase A and acyltransferase 1	lipid biosynthesis
PLB1	Lysophospholipase	lipid biosynthesis
PLB1	Lysophospholipase	lipid biosynthesis
PLA2G2F	Phospholipase A2	lipid biosynthesis
ENPP2	Ectonucleotide pyrophosphatase/phosphodiesterase 2	lipid biosynthesis
LIPI	Lipase I	lipid biosynthesis
PLA2G2A	Phospholipase A2	lipid biosynthesis
SMPD1	Sphingomyelin phosphodiesterase	lipid biosynthesis
DAGLA	Diacylglycerol lipase alpha	lipid biosynthesis
PLBD2	Phospholipase B-like	lipid biosynthesis
PLCG2	1-phosphatidylinositol 4,5-bisphosphate phosphodiesterase gamma	lipid biosynthesis
GPLD1	Glycosyl-phosphatidylinositol-specific phospholipase D	lipid biosynthesis
SMPD2	Sphingomyelin phosphodiesterase 2	lipid biosynthesis
PNLIP	Triacylglycerol lipase	lipid biosynthesis
SMPD5	Sphingomyelin phosphodiesterase	lipid biosynthesis
PLD2	Phospholipase	lipid biosynthesis
PLA1A	Phospholipase A1 member A	lipid biosynthesis
DDHD2	DDHD domain containing 2	lipid biosynthesis
PNPLA3	PNPLA domain-containing protein	lipid biosynthesis
LIPG	Lipoprotein lipase	lipid biosynthesis
LIPG	Lipoprotein lipase	lipid biosynthesis
PLA2G2C	Phospholipase A2 group IIC	lipid biosynthesis
PLA2G4D	Phospholipase A2	lipid biosynthesis
JMJD7	Phospholipase A2	lipid biosynthesis
PNPLA4	PNPLA domain-containing protein	lipid biosynthesis
PLA2G4F	Phospholipase A2	lipid biosynthesis
LIPH	Lipase H	lipid biosynthesis
ABHD1	Abhydrolase domain containing 1	lipid biosynthesis
ABHD12B	Abhydrolase domain containing 12B	lipid biosynthesis
ABHD12B	Abhydrolase domain containing 12B	lipid biosynthesis
LIPA	Lipase A, lysosomal acid type	lipid biosynthesis
PNPLA8	Calcium-independent phospholipase A2-gamma isoform 1	lipid biosynthesis
PLCD1	Phosphoinositide phospholipase C	lipid biosynthesis
ABHD6	Abhydrolase domain containing 6, acylglycerol lipase	lipid biosynthesis
PLCB4	1-phosphatidylinositol 4,5-bisphosphate phosphodiesterase	lipid biosynthesis
PLA2G2D	Phospholipase A2	lipid biosynthesis
PLD1	Phospholipase	lipid biosynthesis
PLA2G6	Phospholipase A2 group VI	lipid biosynthesis
PLA2G5	Phospholipase A2	lipid biosynthesis
PNPLA2	Adipose triglyceride lipase	lipid biosynthesis
ABHD12	2-arachidonoylglycerol hydrolase ABHD12	lipid biosynthesis
ABHD12	2-arachidonoylglycerol hydrolase ABHD12	lipid biosynthesis
PLCE1	Phosphoinositide phospholipase C	lipid biosynthesis
PLA2G2E	Phospholipase A2	lipid biosynthesis
PLA2G10	Phospholipase A2	lipid biosynthesis
ABHD3	Abhydrolase domain containing 3, phospholipase	lipid biosynthesis
ABHD3	Abhydrolase domain containing 3, phospholipase	lipid biosynthesis
ABHD3	Abhydrolase domain containing 3, phospholipase	lipid biosynthesis
PNLIPRP2	Triacylglycerol lipase	lipid biosynthesis
PNLIPRP2	Triacylglycerol lipase	lipid biosynthesis
PNLIPRP2	Triacylglycerol lipase	lipid biosynthesis
ABHD4	Abhydrolase domain containing 4, N-acyl phospholipase B	lipid biosynthesis
PLCH2	Phosphoinositide phospholipase C	lipid biosynthesis
AADAC	Uncharacterized protein	lipid biosynthesis
SMPD3	Sphingomyelin phosphodiesterase	lipid biosynthesis
NAPEPLD	N-acyl-phosphatidylethanolamine-hydrolyzing phospholipase D	lipid biosynthesis
PLCB2	1-phosphatidylinositol 4,5-bisphosphate phosphodiesterase	lipid biosynthesis
PLA2G4A	Phospholipase A2	lipid biosynthesis
ABHD2	Abhydrolase domain containing 2, acylglycerol lipase	lipid biosynthesis
PLAAT3	Phospholipase A and acyltransferase 3	lipid biosynthesis
PLAAT3	Phospholipase A and acyltransferase 3	lipid biosynthesis
PLCB1	1-phosphatidylinositol 4,5-bisphosphate phosphodiesterase	lipid biosynthesis
LIPC	Hepatic triacylglycerol lipase	lipid biosynthesis
LIPC	Hepatic triacylglycerol lipase	lipid biosynthesis
OC90	Uncharacterized protein	lipid biosynthesis
PNPLA1	Patatin like phospholipase domain containing 1	lipid biosynthesis
CEL	Carboxylic ester hydrolase	lipid biosynthesis
CEL	Carboxylic ester hydrolase	lipid biosynthesis
GDPD3	Glycerophosphodiester phosphodiesterase domain containing 3	lipid biosynthesis
PLCG1	Phosphoinositide phospholipase C	lipid biosynthesis
P04416	Phospholipase A2, minor isoenzyme	lipid biosynthesis
PNLIPRP2	Pancreatic lipase-related protein 2	lipid biosynthesis
PNLIPRP2	Pancreatic lipase-related protein 2	lipid biosynthesis
PNLIPRP2	Pancreatic lipase-related protein 2	lipid biosynthesis
